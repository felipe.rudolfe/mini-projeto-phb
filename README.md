
# **API REST**

## **Adicionar rebeldes:**
### **Method:** POST
### **URL:** /rebelde/adicionar
### **JSON:**

#### Requisição:
	{
		"nome": "Luke Skywalker",
		"idade": 32,
		"genero": "MASCULINO"
	}

#### Resposta:
	{
	    "data": {
	        "codigo": 2,
	        "nome": "Luke Skywalker",
	        "idade": 32,
	        "genero": "MASCULINO",
	        "localizacao": {
	            "codigo": 2,
	            "latitude": 969,
	            "longitude": 31,
	            "planeta": "HOTH"
	        },
	        "itens": [
	            {
	                "codigo": 1,
	                "nome": "Arma",
	                "pontos": 4
	            },
	            {
	                "codigo": 2,
	                "nome": "Munição",
	                "pontos": 3
	            },
	            {
	                "codigo": 3,
	                "nome": "Água",
	                "pontos": 2
	            },
	            {
	                "codigo": 4,
	                "nome": "Comida",
	                "pontos": 1
	            }
	        ],
	        "denuncias": null,
	        "traidor": false
	    },
	    "mensagens": [
	        {
	            "tipo": "SUCCESS",
	            "texto": "Seja bem vindo Luke Skywalker a resistência, vá com Deus e um punhado de santo para o planeta HOTH(969,31). Que a força estava com você."
	        }
	    ]
	}

## **Atualizar localização do rebelde:**
### **Method:** GET
### **URL:** /rebelde/viajar/{ID_REBELDE}

#### Resposta:
	{
	    "data": {
	        "codigo": 2,
	        "nome": "Luke Skywalker",
	        "idade": 32,
	        "genero": "MASCULINO",
	        "localizacao": {
	            "codigo": 3,
	            "latitude": 26,
	            "longitude": 968,
	            "planeta": "DAGOBAH"
	        },
	        "itens": [
	            {
	                "codigo": 1,
	                "nome": "Arma",
	                "pontos": 4
	            },
	            {
	                "codigo": 2,
	                "nome": "Munição",
	                "pontos": 3
	            },
	            {
	                "codigo": 3,
	                "nome": "Água",
	                "pontos": 2
	            },
	            {
	                "codigo": 4,
	                "nome": "Comida",
	                "pontos": 1
	            }
	        ],
	        "denuncias": [],
	        "traidor": false
	    },
	    "mensagens": []
	}

## **Reportar o rebelde como um traidor:**
### **Method:** GET
### **URL:** /rebelde/traidor?denunciante={ID_REBELDE}&denunciado={ID_REBELDE_MERCANTE}

#### Resposta:
	{
	    "data": null,
	    "mensagens": [
	        {
	            "tipo": "SUCCESS",
	            "texto": "Temos traidoras entre nós."
	        }
	    ]
	}

## **Negociar itens (PARTE 1 - LISTA ITENS DOS NEGOCIANTES):**
### **Method:** POST
### **URL:** /rebelde/negociar?rebelde={ID_REBELDE}&mercante={ID_REBELDE_MERCANTE}
### **JSON:**

#### Resposta:
	{
	    "data": {
	        "1": [
	            {
	                "codigo": 1,
	                "nome": "Arma",
	                "pontos": 4
	            },
	            {
	                "codigo": 2,
	                "nome": "Munição",
	                "pontos": 3
	            },
	            {
	                "codigo": 3,
	                "nome": "Água",
	                "pontos": 2
	            },
	            {
	                "codigo": 4,
	                "nome": "Comida",
	                "pontos": 1
	            }
	        ],
	        "2": [
	            {
	                "codigo": 1,
	                "nome": "Arma",
	                "pontos": 4
	            },
	            {
	                "codigo": 2,
	                "nome": "Munição",
	                "pontos": 3
	            },
	            {
	                "codigo": 3,
	                "nome": "Água",
	                "pontos": 2
	            },
	            {
	                "codigo": 4,
	                "nome": "Comida",
	                "pontos": 1
	            }
	        ]
	    },
	    "mensagens": [
	        {
	            "tipo": "SUCCESS",
	            "texto": "Deseja algum dos meus itens?"
	        }
	    ]
	}



## **Negociar itens:**
### **Method:** POST
### **URL:** /rebelde/concluirTrocar?rebelde={ID_REBELDE}&mercante={ID_REBELDE_MERCANTE}
### **JSON:**

#### Requisição:
	{
	    "1": [
	        {
	            "codigo": 4,
	            "nome": "Comida",
	            "pontos": "1"
	        },
	        {
	            "codigo": 3,
	            "nome": "Água",
	            "pontos": "2"
	        }
	    ],
	    "2": [
	        {
	            "codigo": 2,
	            "nome": "Munição",
	            "pontos": "3"
	        }
	    ]
	}

#### Resposta:
	{
	    "data": null,
	    "mensagens": [
	        {
	            "tipo": "SUCCESS",
	            "texto": "Troca realizada com sucesso."
	        }
	    ]
	}
