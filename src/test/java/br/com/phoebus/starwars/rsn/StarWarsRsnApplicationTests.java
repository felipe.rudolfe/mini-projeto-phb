package br.com.phoebus.starwars.rsn;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.phoebus.starwars.rsn.comum.tratamento.NegocioException;
import br.com.phoebus.starwars.rsn.modelo.entidade.Localizacao;
import br.com.phoebus.starwars.rsn.modelo.entidade.Rebelde;
import br.com.phoebus.starwars.rsn.modelo.tipo.Genero;
import br.com.phoebus.starwars.rsn.servico.RebeldeService;

@SpringBootTest
@RunWith(SpringRunner.class)
public class StarWarsRsnApplicationTests {

	@Autowired
	private RebeldeService rebeldeService;

	@Test
	public void buscarRebeldePorCodigo() throws NegocioException {
		Rebelde rebelde = this.rebeldeService.buscarPorCodigo(1L);
		Assert.assertEquals(rebelde.getNome(), "Felipe Rudolfe");
	}// buscarRebeldePorCodigo()

	@Test
	public void adicionarRebelde() throws NegocioException {
		Rebelde rebelde = new Rebelde();
		rebelde.setGenero(Genero.MASCULINO);
		rebelde.setIdade(30);
		rebelde.setNome("Usuário Teste");

		this.rebeldeService.adicionarRebelde(rebelde);

		Assert.assertNotNull(rebelde.getCodigo());
	}// adicionarRebelde()

	@Test
	public void viajar() throws NegocioException {
		Rebelde rebelde = this.rebeldeService.buscarPorCodigo(1L);
		Localizacao antiga = rebelde.getLocalizacao();

		rebelde = this.rebeldeService.viajar(1L);
		Localizacao nova = rebelde.getLocalizacao();

		Assert.assertNotEquals(antiga, nova);
	}// viajar()

}