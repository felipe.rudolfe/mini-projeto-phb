package br.com.phoebus.starwars.rsn.comum.tratamento;

import java.util.ArrayList;
import java.util.List;

import br.com.phoebus.starwars.rsn.comum.modelo.dto.Mensagem;

/**
 * @Autor Felipe Rudolfe Carvalho Pinheiro
 * @Data: 26 de out de 2018 às 22:16:34
 */
public class NegocioException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5823495705521885339L;

	private List<Mensagem> mensagens;

	public NegocioException(Mensagem... mensagens) {
		this.mensagens = new ArrayList<Mensagem>();
		for (Mensagem mensagem : mensagens) {
			this.mensagens.add(mensagem);
		}
	}

	public NegocioException(List<Mensagem> mensagens) {
		this.mensagens = mensagens;
	}

	public List<Mensagem> getMensagens() {
		return mensagens;
	}

}