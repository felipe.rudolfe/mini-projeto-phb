package br.com.phoebus.starwars.rsn.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.phoebus.starwars.rsn.modelo.entidade.Item;

/**
 * @Autor 		Felipe Rudolfe Carvalho Pinheiro
 * @Data		27 de out de 2018 às 13:07:03
 */
@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {

	static final String BUSCAR_POR_REBELDE = "SELECT r.itens FROM Rebelde r WHERE r.codigo = :rebelde";

	@Query(BUSCAR_POR_REBELDE)
	public List<Item> buscarPorRebelde(@Param("rebelde") Long rebelde);

}