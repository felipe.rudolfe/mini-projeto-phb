package br.com.phoebus.starwars.rsn.modelo.dto;

import java.util.List;

import br.com.phoebus.starwars.rsn.comum.configurador.Msg;
import br.com.phoebus.starwars.rsn.comum.configurador.MsgRef;
import br.com.phoebus.starwars.rsn.comum.modelo.dto.Mensagem;
import br.com.phoebus.starwars.rsn.comum.modelo.dto.Mensagem.Tipo;
import br.com.phoebus.starwars.rsn.comum.tratamento.NegocioException;
import br.com.phoebus.starwars.rsn.modelo.entidade.Item;
import br.com.phoebus.starwars.rsn.modelo.entidade.Rebelde;

public class Relatorio {

	private float percRebeldes;
	private float percTraidores;
	private float percArmasPorRebelde;
	private int pontosPerdidos;
	private List<Rebelde> rebeldes;

	public void gerarDados(List<Rebelde> rebeldes) throws NegocioException {

		if (rebeldes == null || rebeldes.isEmpty()) {
			throw new NegocioException(new Mensagem(Tipo.WARNING, Msg.get(MsgRef.MSG_NENHUM_REBELDE_CADASTRADO)));
		}

		this.rebeldes = rebeldes;

		int quantTotal = rebeldes.size();
		int quantTraidores = 0;
		int quantRebeldes = 0;
		int quantArmasRebeldes = 0;

		for (Rebelde rebelde : rebeldes) {
			if (rebelde.isTraidor()) {
				quantTraidores++;
				for (Item item : rebelde.getItens()) {
					pontosPerdidos += item.getPontos();
				}// for
			} else {
				quantRebeldes++;
				for (Item item : rebelde.getItens()) {
					if ("Arma".equals(item.getNome())) {
						quantArmasRebeldes++;
					}// if
				}// for
			}// if-else
		}// for

		percRebeldes = quantRebeldes/quantTotal;
		percTraidores = quantTraidores/quantTotal;
		if (quantArmasRebeldes > 0) {
			percArmasPorRebelde = quantRebeldes/quantArmasRebeldes;
		}

	}// gerarDados()

	public float getPercRebeldes() {
		return percRebeldes;
	}

	public float getPercTraidores() {
		return percTraidores;
	}

	public float getPercArmasPorRebelde() {
		return percArmasPorRebelde;
	}

	public float getPontosPerdidos() {
		return pontosPerdidos;
	}

	public List<Rebelde> getRebeldes() {
		return rebeldes;
	}

}