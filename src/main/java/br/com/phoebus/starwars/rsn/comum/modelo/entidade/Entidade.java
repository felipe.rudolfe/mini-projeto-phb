package br.com.phoebus.starwars.rsn.comum.modelo.entidade;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

/**
 * @Autor Felipe Rudolfe Carvalho Pinheiro
 * @Data: 26 de out de 2018 às 22:11:29
 */
@MappedSuperclass
public abstract class Entidade<T> implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public abstract T getCodigo();

	public abstract void setCodigo(T codigo);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected Entidade<T> clone() throws CloneNotSupportedException {
		return (Entidade<T>) super.clone();
	}// clone()

}