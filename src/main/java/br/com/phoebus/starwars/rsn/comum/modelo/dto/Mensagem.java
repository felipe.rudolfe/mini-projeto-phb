package br.com.phoebus.starwars.rsn.comum.modelo.dto;

/**
 * @Autor 		Felipe Rudolfe Carvalho Pinheiro
 * @Data:		26 de out de 2018 às 22:11:56
 */
public class Mensagem extends DTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3416479858603306531L;

	public enum Tipo {
		SUCCESS, 
		WARNING, 
		ERROR
	}
	
	public Mensagem(String msg) {
		this.tipo = Tipo.ERROR;
		this.texto = msg;
	}
	
	public Mensagem(Tipo tipo, String texto) {
		this.tipo = tipo;
		this.texto = texto;
	}

	private Tipo tipo;
	private String texto;

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

}