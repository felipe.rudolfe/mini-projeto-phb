package br.com.phoebus.starwars.rsn.modelo.tipo;

/**
 * @Autor 		Felipe Rudolfe Carvalho Pinheiro
 * @Data		27 de out de 2018 às 13:06:43
 */
public enum Planeta {

	CORUSCANT,
	DAGOBAH,
	HOTH,
	ENDOR,
	NABOO,
	TATOOINE,

}