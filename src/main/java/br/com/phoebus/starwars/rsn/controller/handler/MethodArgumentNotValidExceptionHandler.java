package br.com.phoebus.starwars.rsn.controller.handler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.phoebus.starwars.rsn.comum.modelo.dto.Mensagem;
import br.com.phoebus.starwars.rsn.comum.modelo.dto.Mensagem.Tipo;
import br.com.phoebus.starwars.rsn.comum.modelo.dto.Resposta;

/**
 * @Autor 		Felipe Rudolfe Carvalho Pinheiro
 * @Data		27 de out de 2018 às 13:05:58
 */
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class MethodArgumentNotValidExceptionHandler {

	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Resposta<Object> methodArgumentNotValidException(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		List<org.springframework.validation.FieldError> fieldErrors = result.getFieldErrors();
		return processFieldErrors(fieldErrors);
	}// methodArgumentNotValidException()

	private Resposta<Object> processFieldErrors(List<org.springframework.validation.FieldError> fieldErrors) {
		List<Mensagem> mensagens = new ArrayList<Mensagem>();
		for (org.springframework.validation.FieldError fieldError : fieldErrors) {
			mensagens.add(new Mensagem(Tipo.WARNING, fieldError.getDefaultMessage()));
		} // for
		return new Resposta<Object>(mensagens);
	}// processFieldErrors()

}