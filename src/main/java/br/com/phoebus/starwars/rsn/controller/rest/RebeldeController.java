package br.com.phoebus.starwars.rsn.controller.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.phoebus.starwars.rsn.comum.configurador.Msg;
import br.com.phoebus.starwars.rsn.comum.configurador.MsgRef;
import br.com.phoebus.starwars.rsn.comum.controller.AbstractController;
import br.com.phoebus.starwars.rsn.comum.modelo.dto.Mensagem;
import br.com.phoebus.starwars.rsn.comum.modelo.dto.Mensagem.Tipo;
import br.com.phoebus.starwars.rsn.comum.modelo.dto.Resposta;
import br.com.phoebus.starwars.rsn.modelo.dto.Relatorio;
import br.com.phoebus.starwars.rsn.modelo.entidade.Item;
import br.com.phoebus.starwars.rsn.modelo.entidade.Localizacao;
import br.com.phoebus.starwars.rsn.modelo.entidade.Rebelde;
import br.com.phoebus.starwars.rsn.servico.ItemService;
import br.com.phoebus.starwars.rsn.servico.RebeldeService;

/**
 * @Autor Felipe Rudolfe Carvalho Pinheiro
 * @Data 26 de out de 2018 às 22:14:28
 */
@RestController
@RequestMapping("/rebelde")
public class RebeldeController extends AbstractController {

	@Autowired
	private RebeldeService rebeldeService;

	@Autowired
	private ItemService itemService;

	/**
	 * @throws Exception
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Adicionar os rebeldes a aliança
	 */
	@PostMapping(path = "/adicionar")
	public ResponseEntity<Object> adicionar(@Valid @RequestBody Rebelde rebelde) {

		this.rebeldeService.adicionarRebelde(rebelde);

		Localizacao localizacao = rebelde.getLocalizacao();
		String msg = Msg.get(MsgRef.MSG_CADASTRADO_COM_SUCESSO,
					rebelde.getNome(),
					localizacao.getPlaneta(),
					localizacao.getLatitude(),
					localizacao.getLongitude());

		Mensagem mensagem = new Mensagem(Tipo.SUCCESS, msg);

		return criarResposta(new Resposta<Rebelde>(rebelde, mensagem));
	}// adicionar()

	/**
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Atualizar localização do rebelde
	 */
	@GetMapping(path = "/viajar/{codigo}")
	public ResponseEntity<Object> viajar(@PathVariable Long codigo) throws Exception {
		return criarResposta(new Resposta<Rebelde>(this.rebeldeService.viajar(codigo)));
	}// viajar()

	/**
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Reportar o rebelde como um traidor
	 */
	@GetMapping(path = "/traidor")
	public ResponseEntity<Object> traidor(@QueryParam(value = "denunciante") Long denunciante,
			@QueryParam(value = "denunciado") Long denunciado) throws Exception {
		this.rebeldeService.apontarOTraidor(denunciante, denunciado);
		return criarResposta(new Resposta<String>(new Mensagem(Tipo.SUCCESS, Msg.get(MsgRef.MSG_TEMOS_UM_TRAIDOR))));
	}// traidor()

	/**
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Reportar o rebelde como um traidor
	 */
	@GetMapping(path = "/negociar")
	public ResponseEntity<Object> negociar(@QueryParam(value = "rebelde") Long rebelde,
			@QueryParam(value = "mercante") Long mercante) throws Exception {

		this.rebeldeService.verificarIntegridade(rebelde);
		this.rebeldeService.verificarIntegridade(mercante);

		List<Item> itensRebelde = this.itemService.obterItens(rebelde);
		List<Item> itensMercante = this.itemService.obterItens(mercante);

		Map<Long, List<Item>> map = new HashMap<Long, List<Item>>();
		map.put(rebelde, itensRebelde);
		map.put(mercante, itensMercante);

		return criarResposta(new Resposta<Map<Long, List<Item>>>(map, new Mensagem(Tipo.SUCCESS, Msg.get(MsgRef.MSG_DESEJA_ALGUM_ITEM))));
	}// negociar()

	/**
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Reportar o rebelde como um traidor
	 */
	@PostMapping(path = "/concluirTrocar")
	public ResponseEntity<Object> trocar(@RequestBody Map<Long, List<Item>> map) throws Exception {
		this.rebeldeService.trocar(map);
		return criarResposta(new Resposta<String>(new Mensagem(Tipo.SUCCESS, Msg.get(MsgRef.MSG_TROCAR_SUCESSO))));
	}// negociar()

	/**
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Reportar o rebelde como um traidor
	 */
	@GetMapping(path = "/relatorio")
	public ResponseEntity<Object> gerarRelatorio() throws Exception {

		List<Rebelde> rebeldes = this.rebeldeService.listar();
		Relatorio relatorio = new Relatorio();
		relatorio.gerarDados(rebeldes);

		return criarResposta(new Resposta<Relatorio>(relatorio));
	}// negociar()


}