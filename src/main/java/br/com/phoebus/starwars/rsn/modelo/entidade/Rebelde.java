package br.com.phoebus.starwars.rsn.modelo.entidade;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.phoebus.starwars.rsn.comum.modelo.entidade.Entidade;
import br.com.phoebus.starwars.rsn.modelo.tipo.Genero;

/**
 * @Autor 		Felipe Rudolfe Carvalho Pinheiro
 * @Data		27 de out de 2018 às 13:06:28
 */
@Entity
public class Rebelde extends Entidade<Long> {

	/**
	 *
	 */
	private static final long serialVersionUID = -1769021459229115849L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;

	@NotNull(message = "Informe o seu nome, filho(a)")
	@Size(min = 6, max = 20, message = "Isso não é um nome, filho(a)! Um nome de verdade deve conter entre {min} e {max} caracteres")
	private String nome;

	@NotNull(message = "Informe a sua idade, filho(a)")
	@Min(value = 18, message = "Não temos quem troque suas fraudas, filho(a)! Volte quando tiver {value} anos")
	@Max(value = 65, message = "O asilo fica naquela direção, vovô(ó)! A idade máxima de alistamento é de {value} anos")
	private Integer idade;

	@Enumerated(EnumType.STRING)
	@NotNull(message = "Informe a seu gênero, filho(a)")
	private Genero genero;

	@OneToOne(cascade = CascadeType.ALL)
	private Localizacao localizacao;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "inventario", joinColumns = @JoinColumn(name = "rebelde"), inverseJoinColumns = @JoinColumn(name = "item"))
	private List<Item> itens;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "denuncias", joinColumns = @JoinColumn(name = "denunciado"), inverseJoinColumns = @JoinColumn(name = "denunciante"))
	private List<Rebelde> denuncias;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public Localizacao getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(Localizacao localizacao) {
		this.localizacao = localizacao;
	}

	public List<Rebelde> getDenuncias() {
		return denuncias;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	public void addItens(Iterable<Item> iterable) {
		this.itens = (List<Item>) iterable;
	}

	public void addItem(Item item) {
		if (this.itens == null) {
			this.itens = new ArrayList<Item>();
		}
		this.itens.add(item);
	}

	public void setDenuncias(List<Rebelde> denuncias) {
		this.denuncias = denuncias;
	}

	public void addDenuncia(Rebelde denunciante) {
		if (this.denuncias == null) {
			this.denuncias = new ArrayList<Rebelde>();
		}
		this.denuncias.add(denunciante);
	}

	public Boolean isTraidor() {
		return this.denuncias != null && this.denuncias.size() > 2;
	}

}