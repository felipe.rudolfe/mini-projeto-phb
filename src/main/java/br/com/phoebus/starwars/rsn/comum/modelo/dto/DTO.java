package br.com.phoebus.starwars.rsn.comum.modelo.dto;

import java.io.Serializable;

/**
 * @Autor Felipe Rudolfe Carvalho Pinheiro
 * @Data: 26 de out de 2018 às 22:15:22
 */
public abstract class DTO implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected DTO clone() throws CloneNotSupportedException {
		return (DTO) super.clone();
	}// clone()

}