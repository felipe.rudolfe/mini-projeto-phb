package br.com.phoebus.starwars.rsn.controller.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.phoebus.starwars.rsn.comum.configurador.Msg;
import br.com.phoebus.starwars.rsn.comum.configurador.MsgRef;
import br.com.phoebus.starwars.rsn.comum.modelo.dto.Mensagem;
import br.com.phoebus.starwars.rsn.comum.modelo.dto.Resposta;
import br.com.phoebus.starwars.rsn.comum.tratamento.NegocioException;

/**
 * @Autor Felipe Rudolfe Carvalho Pinheiro
 * @Data: 26 de out de 2018 às 22:11:49
 */
@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

	private static Logger LOGGER = LoggerFactory.getLogger(RestResponseExceptionHandler.class);

	@ExceptionHandler(NegocioException.class)
	protected ResponseEntity<Object> tratarNegocioException(NegocioException ex, WebRequest request) {
		Resposta<String> resposta = new Resposta<String>(ex.getMensagens());
		return handleExceptionInternal(ex, resposta, new HttpHeaders(), HttpStatus.CONFLICT, request);
	}// tratarNegocioException()

	@ExceptionHandler(Exception.class)
	protected ResponseEntity<Object> tratarOutrasExceptions(Exception ex, WebRequest request) {
		LOGGER.info("ERROR: " + ex.getMessage());
		Resposta<String> resposta = new Resposta<String>(new Mensagem(Msg.get(MsgRef.MSG_ERRO_PADRAO)));
		return handleExceptionInternal(ex, resposta, new HttpHeaders(), HttpStatus.CONFLICT, request);
	}// tratarOutrasExceptions()

}