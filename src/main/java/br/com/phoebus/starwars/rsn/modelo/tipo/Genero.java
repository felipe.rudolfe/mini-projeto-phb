package br.com.phoebus.starwars.rsn.modelo.tipo;

/**
 * @Autor 		Felipe Rudolfe Carvalho Pinheiro
 * @Data		27 de out de 2018 às 13:06:35
 */
public enum Genero {

	MASCULINO,
	FEMININO

}