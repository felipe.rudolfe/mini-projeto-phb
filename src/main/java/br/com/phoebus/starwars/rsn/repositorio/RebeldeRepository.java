package br.com.phoebus.starwars.rsn.repositorio;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.phoebus.starwars.rsn.modelo.entidade.Rebelde;

/**
 * @Autor 		Felipe Rudolfe Carvalho Pinheiro
 * @Data		27 de out de 2018 às 13:07:07
 */
@Repository
public interface RebeldeRepository extends CrudRepository<Rebelde, Long> {

}