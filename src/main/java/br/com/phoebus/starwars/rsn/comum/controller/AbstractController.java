package br.com.phoebus.starwars.rsn.comum.controller;

import org.springframework.http.ResponseEntity;

import br.com.phoebus.starwars.rsn.comum.modelo.dto.Resposta;

/**
 * @Autor Felipe Rudolfe Carvalho Pinheiro
 * @Data 27 de out de 2018 às 11:58:04
 */
public class AbstractController {

	protected ResponseEntity<Object> criarResposta(Resposta<?> resposta) {
		return ResponseEntity.ok(resposta);
	}// criarResposta()

}