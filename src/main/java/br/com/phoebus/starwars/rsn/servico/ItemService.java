package br.com.phoebus.starwars.rsn.servico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.phoebus.starwars.rsn.comum.configurador.Msg;
import br.com.phoebus.starwars.rsn.comum.configurador.MsgRef;
import br.com.phoebus.starwars.rsn.comum.modelo.dto.Mensagem;
import br.com.phoebus.starwars.rsn.comum.modelo.dto.Mensagem.Tipo;
import br.com.phoebus.starwars.rsn.comum.tratamento.NegocioException;
import br.com.phoebus.starwars.rsn.modelo.entidade.Item;
import br.com.phoebus.starwars.rsn.repositorio.ItemRepository;

/**
 * @Autor Felipe Rudolfe Carvalho Pinheiro
 * @Data 26 de out de 2018 às 21:15:26
 */
@Service
public class ItemService {

	@Autowired
	private ItemRepository itemRepository;

	/**
	 * @throws NegocioException
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Obter itens
	 */
	public List<Item> obterItens(Long rebelde) throws NegocioException {

		if (rebelde == null) {
			throw new NegocioException(new Mensagem(Tipo.WARNING, Msg.get(MsgRef.MSG_NAO_ENCONTRADO, "Rebelde")));
		}// if

		return this.itemRepository.buscarPorRebelde(rebelde);
	}// obterItens()

}