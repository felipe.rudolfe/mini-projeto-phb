package br.com.phoebus.starwars.rsn.comum.modelo.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @Autor 		Felipe Rudolfe Carvalho Pinheiro
 * @Data:		26 de out de 2018 às 22:11:08
 */
public class Resposta<T> extends DTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7965685895442264196L;

	private T data;
	private List<Mensagem> mensagens = new ArrayList<Mensagem>();

	public Resposta(T data) {
		this.data = data;
	}

	public Resposta(Mensagem... mensagens) {
		for (Mensagem mensagem : mensagens) {
			this.mensagens.add(mensagem);
		}
	}
	
	public Resposta(List<Mensagem> mensagens) {
		this.mensagens = mensagens;
	}

	public Resposta(T data, Mensagem... mensagens) {
		this.data = data;
		for (Mensagem mensagem : mensagens) {
			this.mensagens.add(mensagem);
		}
	}
	
	public Resposta(T data, List<Mensagem> mensagens) {
		this.data = data;
		this.mensagens = mensagens;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public List<Mensagem> getMensagens() {
		return mensagens;
	}

	public void setMensagens(List<Mensagem> mensagens) {
		this.mensagens = mensagens;
	}

}