package br.com.phoebus.starwars.rsn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Autor Felipe Rudolfe Carvalho Pinheiro
 * @Data: 26 de out de 2018 às 22:13:56
 */
@ComponentScan
@SpringBootApplication
public class StarWarsRsnApplication {

	public static void main(String[] args) {
		SpringApplication.run(StarWarsRsnApplication.class, args);
	}

}