package br.com.phoebus.starwars.rsn.servico;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.phoebus.starwars.rsn.comum.configurador.Msg;
import br.com.phoebus.starwars.rsn.comum.configurador.MsgRef;
import br.com.phoebus.starwars.rsn.comum.modelo.dto.Mensagem;
import br.com.phoebus.starwars.rsn.comum.modelo.dto.Mensagem.Tipo;
import br.com.phoebus.starwars.rsn.comum.tratamento.NegocioException;
import br.com.phoebus.starwars.rsn.modelo.entidade.Item;
import br.com.phoebus.starwars.rsn.modelo.entidade.Localizacao;
import br.com.phoebus.starwars.rsn.modelo.entidade.Rebelde;
import br.com.phoebus.starwars.rsn.modelo.tipo.Planeta;
import br.com.phoebus.starwars.rsn.repositorio.ItemRepository;
import br.com.phoebus.starwars.rsn.repositorio.RebeldeRepository;

/**
 * @Autor Felipe Rudolfe Carvalho Pinheiro
 * @Data 26 de out de 2018 às 21:15:26
 */
@Service
public class RebeldeService {

	private static final Random RANDOM = new Random();
	private static final Integer LIMITE = 999;

	@Autowired
	private RebeldeRepository rebeldeRepository;

	@Autowired
	private ItemRepository itemRepository;

	/**
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Adicionar rebelde a aliança:
	 *           <ul>
	 *           <li>Um rebelde deve ter um nome, idade, gênero,
	 *           localização(latitude, longitude e nome, na galáxia, da base ao qual
	 *           faz parte);</li>
	 *           <li>Um rebelde também possui um inventário que deverá ser passado
	 *           na requisição com os recursos em sua posse;</li>
	 *           </ul>
	 */
	@Transactional
	public void adicionarRebelde(Rebelde rebelde) {

		Iterable<Item> itens = this.itemRepository.findAll();
		Localizacao localizacao = getLocalizacaoAleatoria();

		rebelde.addItens(itens);
		rebelde.setLocalizacao(localizacao);

		this.salvar(rebelde);
	}// adicionarRebelde()

	/**
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Atualizar localização do rebelde:
	 *           <ul>
	 *           <li>Um rebelde deve possuir a capacidade de reportar sua última
	 *           localização, armazenando a nova latitude/longitude/nome (não é
	 *           necessário rastrear as localizações, apenas sobrescrever a última é
	 *           o suficiente).</li>
	 *           </ul>
	 */
	@Transactional
	public Rebelde viajar(Long codigo) throws NegocioException {

		if (codigo == null) {
			throw new NegocioException(new Mensagem(Tipo.WARNING, Msg.get(MsgRef.MSG_NAO_ENCONTRADO, "Rebelde")));
		} // if

		this.verificarIntegridade(codigo);
		return this.buscarPorCodigo(codigo);
	}// viajar()

	/**
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Reportar o rebelde como um traidor:
	 *           <ul>
	 *           <li>Eventualmente algum rebelde irá trair a resistência e se aliar
	 *           ao império. Quando isso acontecer, nós precisamos informar que o
	 *           rebelde é um traidor.</li>
	 *           <li>Um traidor não pode negociar os recursos com os demais
	 *           rebeldes, não pode manipular seu inventário, nem ser exibido em
	 *           relatórios.</li>
	 *           <li><strong> Um rebelde é marcado como traidor quando, ao menos,
	 *           três outros rebeldes reportarem a traição. </strong></li>
	 *           <li>Uma vez marcado como traidor, os itens do inventário se tornam
	 *           inacessíveis (eles não podem ser negociados com os demais).</li>
	 *           </ul>
	 */
	@Transactional
	public void apontarOTraidor(Long denunciante, Long denunciado) throws NegocioException {

		if (denunciante == null) {
			throw new NegocioException(new Mensagem(Tipo.WARNING, Msg.get(MsgRef.MSG_COVARDIA)));
		} // if

		if (denunciado == null) {
			throw new NegocioException(new Mensagem(Tipo.WARNING, Msg.get(MsgRef.MSG_DENUNCIA_FALSA)));
		} // if

		Rebelde denuncianteReb = null;
		Rebelde denunciadoReb = null;
		if (denunciado.equals(denunciante)) {
			throw new NegocioException(new Mensagem(Tipo.WARNING, Msg.get(MsgRef.MSG_WTF)));
		} // if

		Optional<Rebelde> denuncianteOpt2 = this.rebeldeRepository.findById(denunciado);
		if (!denuncianteOpt2.isPresent()) {
			throw new NegocioException(new Mensagem(Tipo.WARNING, Msg.get(MsgRef.MSG_FUGIU, "Traidor")));
		} // if

		denuncianteReb = this.buscarPorCodigo(denunciado);
		denunciadoReb = denuncianteOpt2.get();
		denunciadoReb.addDenuncia(denuncianteReb);

		this.salvar(denunciadoReb);
	}// apontarOTraidor()

	/**
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Buscar rebelde por código
	 */
	public Rebelde buscarPorCodigo(Long codigo) throws NegocioException {

		if (codigo == null) {
			throw new NegocioException(new Mensagem(Tipo.WARNING, Msg.get(MsgRef.MSG_NAO_ENCONTRADO, "Rebelde")));
		} // if

		Rebelde retorno = null;
		Optional<Rebelde> optional = this.rebeldeRepository.findById(codigo);
		if (optional.isPresent()) {
			retorno = optional.get();
			retorno.setLocalizacao(this.getLocalizacaoAleatoria());
			this.salvar(retorno);
		} else {
			throw new NegocioException(new Mensagem(Tipo.WARNING, Msg.get(MsgRef.MSG_NAO_ENCONTRADO, "Rebelde")));
		} // if

		return retorno;
	}// buscarPorCodigo()

	/**
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Salvar rebelde
	 */
	public void salvar(Rebelde rebelde) {
		this.rebeldeRepository.save(rebelde);
	}// salvar()

	/**
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Gerar randomicamente a localização
	 */
	private Localizacao getLocalizacaoAleatoria() {

		Localizacao localizacao = new Localizacao();
		localizacao.setLatitude(RANDOM.nextInt(LIMITE));
		localizacao.setLongitude(RANDOM.nextInt(LIMITE));
		localizacao.setPlaneta(getPlanetaRandom());

		return localizacao;
	}// getLocalizacaoAleatoria()

	/**
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Obter arandomicamente o planeta
	 */
	public static Planeta getPlanetaRandom() {
		return Planeta.values()[RANDOM.nextInt(Planeta.values().length)];
	}// getPlanetaRandom()

	/**
	 * @throws NegocioException
	 * @Autor Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo Efetuar troca de itens
	 */
	@Transactional
	public void trocar(Map<Long, List<Item>> map) throws NegocioException {

		Long codRebelde = null;
		Long codMercante = null;
		List<Item> itensRebelde = null;
		List<Item> itensMercante = null;

		int valorItens = 0;
		for (Long codigo : map.keySet()) {
			List<Item> itens = map.get(codigo);
			if (valorItens > 0) {
				codMercante = codigo;
				itensMercante = itens;
				int valorItens2 = 0;
				for (Item item : itens) {
					valorItens2 += item.getPontos();
				} // for
				if (valorItens != valorItens2) {
					throw new NegocioException(new Mensagem(Tipo.WARNING, Msg.get(MsgRef.MSG_TROCA_INJUSTA)));
				} // if
			} else {
				codRebelde = codigo;
				itensRebelde = itens;
				for (Item item : itens) {
					valorItens += item.getPontos();
				} // for
			} // if-else
		} // for

		this.verificarIntegridade(codRebelde);
		this.verificarIntegridade(codMercante);

		Rebelde rebelde = this.buscarPorCodigo(codRebelde);
		Rebelde mercante = this.buscarPorCodigo(codMercante);

		trocarItensIventario(rebelde, itensRebelde, itensMercante);
		trocarItensIventario(mercante, itensMercante, itensRebelde);

		this.salvar(rebelde);
		this.salvar(mercante);
	}// trocar()

	private void trocarItensIventario(Rebelde rebelde, List<Item> entrada, List<Item> saida) throws NegocioException {
		for (Item it : saida) {
			FOR_2: for (Item item : rebelde.getItens()) {
				if (item.getCodigo().equals(it.getCodigo())) {
					rebelde.getItens().remove(item);
					break FOR_2;
				} // if
			} // for
		} // for
		for (Item item : entrada) {
			Optional<Item> it = this.itemRepository.findById(item.getCodigo());
			if (it.isPresent()) {
				rebelde.addItem(it.get());
			} else {
				throw new NegocioException(new Mensagem(Tipo.WARNING, Msg.get(MsgRef.MSG_NAO_RELACIONADO)));
			} // if-else
		} // for
	}// trocarItensIventario()

	public void verificarIntegridade(Long codigo) throws NegocioException {
		Rebelde rebelde = this.buscarPorCodigo(codigo);
		if (rebelde.isTraidor()) {
			throw new NegocioException(new Mensagem(Tipo.WARNING, Msg.get(MsgRef.MSG_IS_TRAIDOR, rebelde.getNome())));
		}// if
	}// verificarIntegridade()

	/**
	 * @Autor 		Felipe Rudolfe Carvalho Pinheiro
	 * @Objetivo	Listar todos os rebeldes
	 */
	public List<Rebelde> listar() {
		Iterable<Rebelde> list = this.rebeldeRepository.findAll();
		List<Rebelde> rebeldes = (List<Rebelde>) list;
		return rebeldes;
	}// listar()

}